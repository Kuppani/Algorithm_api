const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = ['questions', 'behaviour', 'combinations'];
const algorithmSchema = {
    hashKey: 'questionId',
    rangeKey: 'storeName',
    timestamps: true,
    schema: Joi.object({
        questionId: Joi.string(), 
        storeName: Joi.string(),
        questions: Joi.object(),
        behaviour: Joi.string(),
        combinations: Joi.object(),
    }).optionalKeys(optionalParams).unknown(true)
}


const attToGet = ['questionId','storeName','questions', 'behaviour', 'combinations'];
const attToQuery = ['questionId','questions', 'behaviour', 'combinations'];
const optionsObj = {
    attToGet,
    attToQuery
};
const Algorithm = SchemaModel(algorithmSchema, optionsObj);

module.exports = Algorithm;