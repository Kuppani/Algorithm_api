const router = require('express').Router();
const Algorithm = require('../../models/algorithm');
const Product = require('../../models/product');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const statusMsg = require('../../methods/statusMsg');
const config = require('../../config/config');


router.get('/', (req, res) => {
	res.send('This is the api route for Algorithm management');
});

router.post('/getquestion', (req, res) => {
	var token = req.body.token || req.headers['token'];

	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
				const table = `${decode.storeName}_algorithm_data`;
				const bodyParams = Methods.initializer(req, Algorithm);
				bodyParams['storeName']=decode.storeName;

				console.log(bodyParams);
				Algorithm.selectTable(table);
				Algorithm.getItem(bodyParams, {}).then((algorithm) => {
					res.json(algorithm);
				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
				})

			}
		})
	}
	else {
		res.send("please send a token");
	}
});

router.post('/getbasket', (req, res) => {
	var token = req.body.token || req.headers['token'];

	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
				const table = `${decode.storeName}_store_data`;
				const bodyParams = Methods.initializer(req, Product);

                Product.selectTable(table);
				Product.getItem(bodyParams, {}).then((basket) => {
					res.json(basket);
				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
				})

			}
		})
	}
	else {
		res.send("please send a token");
	}
});

router.post('/addquestion', (req, res) => {
	var token = req.body.token || req.headers['token'];

	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
				        const table = `${decode.storeName}_algorithm_data`;
						const bodyParams = Methods.initializer(req, Algorithm);
						bodyParams['questionId'] = Date.now();
						bodyParams['storeName'] = decode.storeName;

						Algorithm.selectTable(table);
						Algorithm.createItem(bodyParams, {
							table: decode.storeName + "_algorithm_data",
							overwrite: false
						}).then((algorithm) =>{
							res.send(algorithm);
						}).catch((err) => {
							res.send(statusMsg.errorResponse(err));
						})

			}

		});

	}else {
		res.send("please send a token");
	}
});

router.get('/editquestion/:questionId', (req, res) => {
	const token = req.body.token || req.headers['token'];
	const params = req.params;

		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = `${decode.storeName}_algorithm_data`;
					params['storeName'] = decode.storeName;

                    Algorithm.selectTable(table);
					Algorithm.getItem(params, {}).then((algorithm) => {
						res.json(algorithm);
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}	
});

router.put('/updatequestion',(req, res) => {
	var token = req.body.token || req.headers['token'];
	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}else {
				       const table = `${decode.storeName}_algorithm_data`;
					   const bodyParams = Methods.initializer(req, Algorithm);
					   //console.log(bodyParams);
					// const putparams = {
					// 	  questionId:req.body.questionId,
					// 	  storeName:decode.storeName,
					// 	  questions:req.body.questions,
					// 	  behaviour:req.body.behaviour,
					// 	  combinations:req.body.combinations
					// }
				        Algorithm.selectTable(table);
						Algorithm.updateItem(bodPparams, {
							table: decode.storeName + "_algorithm_data",
							overwrite: false
						}).then((algorithm) =>{
							res.send(algorithm);
						}).catch((err) => {
							res.send(statusMsg.errorResponse(err));
						})
			}
		})
	}else {
		res.send("please send a token");
	}
});

router.delete('/deletequestion', (req, res) => {
	var token = req.body.token || req.headers['token'];
	const bodyParams = req.body;
	
	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {

				const table = `${decode.storeName}_algorithm_data`;
				bodyParams['storeName'] = decode.storeName;

				Algorithm.selectTable(table);
				Algorithm.deleteItem(bodyParams, { ReturnValues: 'ALL_OLD' })
				.then((removeData) =>{
					res.send(removeData);
				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
				})
			}
		})
	}
	else {
		res.send("please send a token");
	}
});

module.exports = router;
